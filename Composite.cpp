#include "Composite.h"
#include <iostream>

void Component::Dump() const
{
	std::cout << "{Component} / " << Data << " /" << '\n';
}

void AbilityCompose::Dump() const
{
	std::cout << "{Branch} / " << Data << " /" << '\n';
	for (Component* t : Children)
	{
		std::cout << "---- ";
		t->Dump();
	}
}