#pragma once
#include <iostream>
#include <string>

enum class Keys
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	FIRE,
	JUMP,
	SHIFT
};

class Actor
{
public:
	void MoveForward() { std::cout << "Moving forward by 100inc\n"; };
	void MoveLeft() { std::cout << "Moving left by 100inc\n"; };
	void MoveRight() { std::cout << "Moving right by 100inc\n"; };
	void MoveBackwd() { std::cout << "Moving backward by 100inc\n"; };

	void Fire() { std::cout << "Fire!!!\n"; };
	void Shift() { std::cout << "Shifting\n"; };
};

/* Facade for Actor object */
class ActorFacade
{
public:
	Actor actor;
public:
	void HandleInput(const Keys& key)
	{
		switch (key)
		{
		case Keys::UP:
			actor.MoveForward();
			break;
		case Keys::DOWN:
			actor.MoveBackwd();
			break;
		case Keys::LEFT:
			actor.MoveLeft();
			break;
		case Keys::RIGHT:
			actor.MoveRight();
			break;
		case Keys::FIRE:
			actor.Fire();
			break;
		case Keys::SHIFT:
			actor.Shift();
			break;
		default:
			break;
		}
	}
};