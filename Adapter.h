#pragma once

class IEnemy
{
public:
	virtual ~IEnemy() = default;
	virtual void Attack() const = 0;
protected:
	IEnemy() = default;
};

// have default interface of attack
class MilleEnemy : public IEnemy
{
public:
	void Attack() const override;
};

// have cating instead of attack
class CastingEnemy
{
public:
	void CastSpell() const;
};

// adapter for mag enemy to use Attack
class MagEnemy : public IEnemy, public CastingEnemy
{
public:
	void Attack() const override;
};