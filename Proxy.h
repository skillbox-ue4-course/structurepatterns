#pragma once
#include <iostream>
#include <set>

class IDatabase
{
public:
	virtual void Dump(const std::string& key, const std::string& val) const = 0;
	virtual std::string Read(const std::string& key) const = 0;
protected:
	~IDatabase() {};
};

class Database : public IDatabase
{
public:
	const std::string& DB;
public:
	Database(const std::string& db_name) :
		DB(db_name)
	{}

	void Dump(const std::string& key, const std::string& val) const override
	{
		std::cout << "Try open db...\n";
		std::cout << "Writing to db.....\n";
		std::cout << "Close db....\n";
	}

	std::string Read(const std::string& key) const override
	{
		std::cout << "Try open db...\n";
		std::cout << "Reading to db.....\n";
		std::cout << "Close db....\n";
		return "result";
	}
};

class DBProxy : public IDatabase
{
private:
	const Database db;
	std::set<std::string, std::string> hash;

public:

	DBProxy(const std::string& db_name)
		: db(db_name)
	{}

	void Dump(const std::string& key, const std::string& val) const override
	{
		db.Dump(key, val);
	}

	std::string Read(const std::string& key) const override
	{
		std::cout << "Proxy: Check request in hash...\n";
		std::string res = db.Read(key);
		std::cout << "Proxy: Write result to hash for futher reuse\n";
		return res;
	}
};