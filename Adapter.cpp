#include "Adapter.h"
#include <iostream>

void MilleEnemy::Attack() const
{
	std::cout << "Enemy attack with a sword\n";
}

void CastingEnemy::CastSpell() const
{
	std::cout << "Enemy casting meteor. Run...\n";
}

void MagEnemy::Attack() const
{
	CastSpell();
}