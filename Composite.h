#pragma once
#include <vector>
#include <string>

// main item
class Component
{
protected:
	Component* Parent = nullptr;
	bool bActive = false;
	const std::string Data;

public:
	Component(const std::string& desc)
		: Data(desc)
	{}

	virtual ~Component() {};

	// check if it simple node
	virtual bool IsComposite() const
	{
		return false;
	}

	virtual void Learn()
	{
		bActive = true;
	}

	bool IsActive() const
	{
		return bActive;
	}

	void SetParent(Component* p)
	{
		Parent = p;
	}

	Component* GetParent() const
	{
		return Parent;
	}

	virtual void Dump() const;

	// do nothing here
	virtual void Add(Component* component) {}
};

class AbilityCompose : public Component
{
private:
	std::vector<Component*> Children;

public:
	AbilityCompose(const std::string& data) :
		Component(data)
	{}

	// now it composite
	bool IsComposite() const override
	{
		return true;
	}

	void Learn() override
	{
		for (Component* c : Children)
		{
			c->Learn();
		}
		bActive = true;
	}

	// create branch
	void Add(Component* component) override
	{
		component->SetParent(this);
		Children.push_back(component);
	}

	void Dump() const override;
};