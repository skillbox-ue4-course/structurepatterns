﻿// StructurePatterns.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Facade.h"
#include "Proxy.h"
#include "Composite.h"
#include "Adapter.h"

void TestFacadePattern()
{
	ActorFacade af;
	af.HandleInput(Keys::UP);
	af.HandleInput(Keys::UP);
	af.HandleInput(Keys::LEFT);
	af.HandleInput(Keys::FIRE);
	af.HandleInput(Keys::LEFT);
	af.HandleInput(Keys::UP);
	af.HandleInput(Keys::RIGHT);
	af.HandleInput(Keys::FIRE);
	af.HandleInput(Keys::SHIFT);
	af.HandleInput(Keys::DOWN);
}

void TestProxyPattern()
{
	DBProxy db("inventory");
	db.Dump("helm", "type-1");
	db.Dump("sword", "excallibur");
	db.Read("sword");
}

void TestCompositePattern()
{
	Component* ab_1 = new Component("firebol");
	Component* ab_2 = new Component("firewall");
	Component* ab_3 = new Component("Meteor");
	Component* ab_4 = new Component("VulkanBurst");

	Component* FireMageBranch = new AbilityCompose("FireMag");
	FireMageBranch->Add(ab_1);
	FireMageBranch->Add(ab_2);
	FireMageBranch->Add(ab_3);
	FireMageBranch->Add(ab_4);

	ab_1->Dump();
	ab_2->Dump();
	ab_3->Dump();
	ab_4->Dump();

	FireMageBranch->Dump();

	delete ab_1;
	delete ab_2;
	delete ab_3;
	delete ab_4;

	delete FireMageBranch;
}

void TestAdapter()
{
	IEnemy* orc = new MilleEnemy();
	IEnemy* loc = new MagEnemy();

	orc->Attack();
	loc->Attack();

	delete orc;
	delete loc;
}

int main()
{
	//TestFacadePattern();
	//TestProxyPattern();
	//TestCompositePattern();
	TestAdapter();
}